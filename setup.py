import os
from setuptools import setup, find_packages

README_PATH = os.path.join(os.path.abspath(os.path.dirname(__file__)),
                           'README.rst')

dependencies = [
    'django-admin-extend==0.0.1tmr'
]

dependency_links = [
    'git+git://github.com/pbs/django-admin-extend.git@0.0.1tmr#egg=django-amin-extend-0.0.1tmr',
]

setup(
    name='django-cms-smartsnippets',
    version='0.1.21tmr',
    description='Parametrizable Django CMS snippets.',
    long_description=open(README_PATH, 'r').read(),
    author='Sever Banesiu',
    author_email='banesiu.sever@gmail.com',
    packages=find_packages(),
    include_package_data=True,
    license='BSD License',
    install_requires=dependencies,
    dependency_links=dependency_links,
    setup_requires=[],
)
